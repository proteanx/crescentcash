package app.crescentcash.src.wallet

import android.content.Context
import android.media.MediaPlayer
import android.os.CountDownTimer
import android.os.Looper
import android.os.Vibrator
import android.text.TextUtils
import android.view.View
import android.os.Handler
import androidx.lifecycle.Observer

import com.bitcoin.slpwallet.Network
import com.bitcoin.slpwallet.SLPWallet
import com.bitcoin.slpwallet.presentation.BalanceInfo
import com.google.common.collect.ImmutableList
import com.google.common.util.concurrent.FutureCallback
import com.google.common.util.concurrent.Futures
import com.google.common.util.concurrent.ListenableFuture

import org.bitcoinj.core.listeners.DownloadProgressTracker
import org.bitcoinj.kits.WalletAppKit
import org.bitcoinj.params.MainNetParams
import org.bitcoinj.params.TestNet3Params
import org.bitcoinj.protocols.payments.PaymentProtocol
import org.bitcoinj.protocols.payments.PaymentSession
import org.bitcoinj.script.ScriptBuilder
import org.bitcoinj.utils.BriefLogFormatter
import org.bitcoinj.utils.MonetaryFormat
import org.bitcoinj.utils.Threading
import org.bitcoinj.wallet.DeterministicSeed
import org.bitcoinj.wallet.SendRequest
import org.bitcoinj.wallet.Wallet

import java.io.File
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat

import app.crescentcash.src.MainActivity
import app.crescentcash.src.R
import app.crescentcash.src.net.NetHelper
import app.crescentcash.src.ui.UIHelper
import app.crescentcash.src.uri.URIHelper
import app.crescentcash.src.utils.Constants
import com.google.protobuf.ByteString
import com.vdurmont.emoji.EmojiParser
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.send.*
import org.bitcoinj.core.*
import org.bitcoinj.crypto.KeyCrypterScrypt
import org.bitcoinj.protocols.payments.PaymentProtocolException
import org.bitcoinj.wallet.Protos
import org.spongycastle.crypto.params.KeyParameter
import org.spongycastle.util.encoders.Hex
import java.net.InetSocketAddress
import java.net.Proxy
import java.nio.charset.StandardCharsets
import java.text.DecimalFormatSymbols
import java.util.*
import java.util.concurrent.ExecutionException
import java.util.concurrent.Executor

class WalletHelper {
    private val walletDir: File = File(MainActivity.INSTANCE.applicationInfo.dataDir)

    var parameters: NetworkParameters = if (Constants.IS_PRODUCTION) MainNetParams.get() else TestNet3Params.get()
    var walletKit: WalletAppKit? = null
    var slpWallet: SLPWallet? = null
    lateinit var serializedXpub: String
    private val uiHelper: UIHelper
    private val netHelper: NetHelper
    var registeredTxHash = "null"
    var registeredBlock = "null"
    var registeredBlockHash = "null"
    lateinit var timer: CountDownTimer
    var downloading: Boolean = false
    var displayUnits: String
    var sendType: String
    var maximumAutomaticSend: Float
    lateinit var balances: List<BalanceInfo>
    var tokenList = ArrayList<Map<String, String>>()
    lateinit var tokenListInfo: ArrayList<BalanceInfo>
    lateinit var currentTokenId: String
    var currentTokenPosition: Int = 0
    var torProxy: Proxy? = null
    var aesKey: KeyParameter? = null

    val wallet: Wallet
        get() = walletKit!!.wallet()

    lateinit var bchToSend: String

    init {
        BriefLogFormatter.init()

        uiHelper = MainActivity.INSTANCE.uiHelper
        netHelper = MainActivity.INSTANCE.netHelper

        displayUnits = MainActivity.INSTANCE.prefs.getString("displayUnit", MonetaryFormat.CODE_BTC) as String
        sendType = MainActivity.INSTANCE.prefs.getString("sendType", displayUnits) as String
        maximumAutomaticSend = MainActivity.INSTANCE.prefs.getFloat("maximumAutomaticSend", 0.00f)

        if(useTor)
        {
            torProxy = Proxy(Proxy.Type.SOCKS, InetSocketAddress("127.0.0.1", 9050))
        }

        val decimalFormatter = DecimalFormat("#.########", DecimalFormatSymbols(Locale.US))
        this.uiHelper.autoSendAmount.text = decimalFormatter.format(maximumAutomaticSend.toString().toFloat())
        println(sendType)
    }

    fun setupWalletKit(seed: DeterministicSeed?, cashAcctName: String, verifyingRestore: Boolean) {
        setBitcoinSDKThread()

        walletKit = object : WalletAppKit(parameters, walletDir, Constants.WALLET_NAME) {
            override fun onSetupCompleted() {
                peerGroup().setBloomFilterFalsePositiveRate(0.01)
                peerGroup().isBloomFilteringEnabled = true
                wallet().allowSpendingUnconfirmedTransactions()
                setupWalletListeners(wallet())
                serializedXpub = wallet.watchingKey.serializePubB58(parameters)

                if (MainActivity.isNewUser) {
                    val address = wallet().currentReceiveAddress().toString()

                    println("Registering...")
                    MainActivity.INSTANCE.prefs.edit().putBoolean("isNewUser", false).apply()
                    if(Constants.IS_PRODUCTION) netHelper.registerCashAccount(cashAcctName, address)
                } else {
                    val keychainSeed = wallet().keyChainSeed

                    if(keychainSeed.isEncrypted)
                    {
                        /*
                        If the saved setting we got is false, but our wallet is encrypted, then we set our saved setting to true.
                         */
                        if(!encrypted)
                        {
                            encrypted = true
                            uiHelper.encryptWalletSwitch.isChecked = encrypted
                            MainActivity.INSTANCE.prefs.edit().putBoolean("useEncryption", encrypted).apply()
                        }

                        MainActivity.INSTANCE.runOnUiThread { uiHelper.displayUnlockScreen() }
                    }
                    else
                    {
                        /*
                        If the saved setting we got is true, but our wallet is unencrypted, then we set our saved setting to false.
                         */
                        if(encrypted)
                        {
                            encrypted = false
                            uiHelper.encryptWalletSwitch.isChecked = encrypted
                            MainActivity.INSTANCE.prefs.edit().putBoolean("useEncryption", encrypted).apply()
                        }
                    }

                    if (verifyingRestore) {
                        try {
                            val cashAcctAddress = org.bitcoinj.net.NetHelper().getCashAccountAddress(parameters, cashAcctName)
                            val cashAcctEmoji = netHelper.getCashAccountEmoji(cashAcctName)
                            println(cashAcctAddress)
                            var accountAddress: Address? = null

                            if (Address.isValidCashAddr(parameters, cashAcctAddress)) {
                                accountAddress = AddressFactory.create().getAddress(MainActivity.INSTANCE.walletHelper.parameters, cashAcctAddress)
                            } else if (Address.isValidLegacyAddress(parameters, cashAcctAddress)) {
                                accountAddress = Address.fromBase58(MainActivity.INSTANCE.walletHelper.parameters, cashAcctAddress)
                            } else {
                                uiHelper.showToastMessage("No address found!")
                            }

                            if (accountAddress != null) {
                                val isAddressMine = isAddressMine(accountAddress.toString())

                                if (isAddressMine) {
                                    val pref = MainActivity.INSTANCE.prefs.edit()
                                    pref.putString("cashAccount", cashAcctName)
                                    pref.putString("cashEmoji", cashAcctEmoji)
                                    pref.putBoolean("isNewUser", false)
                                    pref.apply()

                                    MainActivity.INSTANCE.runOnUiThread {
                                        uiHelper.restore_wallet.visibility = View.GONE
                                        uiHelper.displayDownloadContent(true)
                                        uiHelper.myEmoji.text = cashAcctEmoji
                                        uiHelper.showToastMessage("Verified!")
                                        uiHelper.refresh()
                                    }
                                } else {
                                    walletKit!!.stopAsync()
                                    walletKit = null
                                    MainActivity.INSTANCE.runOnUiThread { uiHelper.showToastMessage("This is not your Cash Account!") }
                                }
                            }
                        } catch (e: NullPointerException) {
                            if (walletKit != null) {
                                walletKit!!.stopAsync()
                                walletKit = null
                            }
                            MainActivity.INSTANCE.runOnUiThread { uiHelper.showToastMessage("Cash Account not found.") }
                        }

                    } else {
                        val hasSLPWallet = MainActivity.INSTANCE.prefs.getBoolean("hasSLPWallet", false)

                        if (!hasSLPWallet) {
                            val mainHandler = Handler(Looper.getMainLooper())

                            val myRunnable = { setupSLPWallet(null, false) }
                            mainHandler.post(myRunnable)
                        }

                        if (seed == null)
                            uiHelper.refresh()
                    }
                }
            }
        }

        walletKit!!.setDownloadListener(object : DownloadProgressTracker() {
            override fun progress(pct: Double, blocksSoFar: Int, date: Date) {
                super.progress(pct, blocksSoFar, date)
                val percentage = pct.toInt()
                MainActivity.INSTANCE.runOnUiThread {
                    uiHelper.displayPercentage(percentage)
                }
            }

            override fun doneDownload() {
                super.doneDownload()
                MainActivity.INSTANCE.runOnUiThread {
                    uiHelper.displayDownloadContent(false)
                    uiHelper.refresh()
                }
            }
        })

        if (seed != null)
            walletKit!!.restoreWalletFromSeed(seed)

        walletKit!!.setBlockingStartup(false)
        walletKit!!.startAsync()
    }

    private fun isAddressMine(address: String): Boolean {
        val addressObj = AddressFactory.create().getAddress(parameters, address)

        return wallet.isPubKeyHashMine(addressObj.hash160)
    }

    private fun setBitcoinSDKThread() {
        val handler = Handler()
        Threading.USER_THREAD = Executor { handler.post(it) }
    }

    fun getBalance(wallet: Wallet): Coin {
        return wallet.getBalance(Wallet.BalanceType.ESTIMATED)
    }

    fun getSLPWallet(): SLPWallet {
        return slpWallet!!
    }

    fun send() {
        if(!this.uiHelper.isDisplayingDownload) {
            uiHelper.btnSend_AM.isEnabled = false
            val amount = uiHelper.amount
            val amtDblToFrmt: Double

            amtDblToFrmt = if (!TextUtils.isEmpty(amount)) {
                java.lang.Double.parseDouble(amount)
            } else {
                0.0
            }

            val formatter = DecimalFormat("#.########", DecimalFormatSymbols(Locale.US))
            val amtDblFrmt = formatter.format(amtDblToFrmt)
            var amtToSend = java.lang.Double.parseDouble(amtDblFrmt)

            if (sendType == "USD" || sendType == "EUR" || sendType == "AUD") {
                object : Thread() {
                    override fun run() {
                        if (sendType == "USD") {
                            val usdToBch = amtToSend / MainActivity.INSTANCE.netHelper.price
                            bchToSend = formatter.format(usdToBch)
                            processPayment()
                        }

                        if (sendType == "EUR") {
                            val usdToBch = amtToSend / MainActivity.INSTANCE.netHelper.priceEur
                            bchToSend = formatter.format(usdToBch)
                            processPayment()
                        }

                        if (sendType == "AUD") {
                            val usdToBch = amtToSend / MainActivity.INSTANCE.netHelper.priceAud
                            bchToSend = formatter.format(usdToBch)
                            processPayment()
                        }
                    }
                }.start()
            } else {
                if (sendType == MonetaryFormat.CODE_BTC) {
                    println("No formatting needed")
                    bchToSend = formatter.format(amtToSend)
                }

                if (sendType == MonetaryFormat.CODE_MBTC) {
                    val mBTCToSend = amtToSend
                    amtToSend = mBTCToSend / 1000.0
                    bchToSend = formatter.format(amtToSend)
                }

                if (sendType == MonetaryFormat.CODE_UBTC) {
                    val uBTCToSend = amtToSend
                    amtToSend = uBTCToSend / 1000000.0
                    bchToSend = formatter.format(amtToSend)
                }

                if (sendType == "sats") {
                    val satsToSend = amtToSend.toLong()
                    amtToSend = satsToSend / 100000000.0
                    bchToSend = formatter.format(amtToSend)
                }

                processPayment()
            }
        }
    }

    fun processScanOrPaste(text: String)
    {
        val uri = URIHelper(text, true)
        val address = uri.address

        if (address.startsWith("http")) {
            MainActivity.INSTANCE.runOnUiThread {
                MainActivity.INSTANCE.uiHelper.displayRecipientAddress(address)
                MainActivity.INSTANCE.uiHelper.sendType.setSelection(0)
            }

            MainActivity.INSTANCE.walletHelper.sendType = MainActivity.INSTANCE.walletHelper.displayUnits
            MainActivity.INSTANCE.prefs.edit().putString("sendType", MainActivity.INSTANCE.walletHelper.sendType).apply()

            this.getBIP70Data(address)
        }
    }

    private fun getBIP70Data(url: String)
    {
        object : Thread() {
            override fun run() {
                try {
                    val future: ListenableFuture<PaymentSession> = PaymentSession.createFromUrl(url)

                    val session = future.get()

                    val amountWanted = session.value

                    val amountFormatted = URIHelper().processSendAmount(amountWanted.toPlainString())
                    val amountAsFloat = amountFormatted.toFloat()
                    MainActivity.INSTANCE.runOnUiThread { uiHelper.amountText.text = amountFormatted }

                    if (amountAsFloat <= MainActivity.INSTANCE.walletHelper.maximumAutomaticSend) {
                        MainActivity.INSTANCE.runOnUiThread { MainActivity.INSTANCE.uiHelper.btnSend_AM.completeSlider() }
                        MainActivity.INSTANCE.walletHelper.send()
                    }
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                } catch (e: ExecutionException) {
                    e.printStackTrace()
                } catch (e: PaymentProtocolException) {
                    e.printStackTrace()
                }

            }
        }.start()
    }

    private fun processPayment()
    {
        println(bchToSend)
        if(!TextUtils.isEmpty(uiHelper.amount) && !TextUtils.isEmpty(bchToSend) && MainActivity.INSTANCE.uiHelper.amountText.text != null && bchToSend != "")
        {
            val amtCheckVal = java.lang.Double.parseDouble(Coin.parseCoin(bchToSend).toPlainString())

            if (TextUtils.isEmpty(uiHelper.recipient)) {
                throwSendError("Please enter a recipient.")
            } else if (amtCheckVal < 0.00001) {
                throwSendError("Enter a valid amount. Minimum is 0.00001 BCH")
            } else if (MainActivity.INSTANCE.walletHelper.getBalance(MainActivity.INSTANCE.walletHelper.wallet).isLessThan(Coin.parseCoin(amtCheckVal.toString()))) {
                throwSendError("Insufficient balance!")
            } else {
                val recipientText = uiHelper.recipient
                val uri = URIHelper(recipientText, false)
                val address = uri.address

                val amount = if(uri.amount != "null")
                    uri.amount
                else
                    "null"

                if(amount != "null")
                    bchToSend = amount

                if (address.startsWith("http"))
                {
                    MainActivity.INSTANCE.runOnUiThread {
                        MainActivity.INSTANCE.uiHelper.displayRecipientAddress(address)
                        MainActivity.INSTANCE.uiHelper.sendType.setSelection(0)
                    }
                    MainActivity.INSTANCE.walletHelper.sendType = MainActivity.INSTANCE.walletHelper.displayUnits
                    MainActivity.INSTANCE.prefs.edit().putString("sendType", MainActivity.INSTANCE.walletHelper.sendType).apply()

                    this.processBIP70(address)
                }
                else if(address.startsWith("+"))
                {
                    val rawNumber = URIHelper().getRawPhoneNumber(address)
                    val numberString = rawNumber.replace("+", "")
                    var amtToSats = java.lang.Double.parseDouble(bchToSend)
                    val satFormatter = DecimalFormat("#", DecimalFormatSymbols(Locale.US))
                    amtToSats *= 100000000
                    val sats = satFormatter.format(amtToSats).toInt()
                    val url = "https://pay.cointext.io/p/$numberString/$sats"
                    println(url)
                    this.processBIP70(url)
                }
                else
                {
                    if (address.contains("#")) {
                        val toAddressFixed = EmojiParser.removeAllEmojis(address)
                        val toAddressStripped = toAddressFixed.replace("; ", "")
                        sendCoins(bchToSend, toAddressStripped)
                    } else {
                        try {
                            sendCoins(bchToSend, address)
                        } catch (e: AddressFormatException) {
                            e.printStackTrace()
                            throwSendError("Invalid address!")
                        }
                    }
                }
            }
        }
        else
        {
            throwSendError("Please enter an amount.")
        }
    }

    private fun sendCoins(amount: String, toAddress: String) {
        if (toAddress.contains("#") || Address.isValidCashAddr(parameters, toAddress) || Address.isValidLegacyAddress(parameters, toAddress)) {
            object : Thread() {
                override fun run() {
                    val recipientAddress = uiHelper.recipient
                    val coinAmt = Coin.parseCoin(amount)

                    if (coinAmt.getValue() > 0.0) {
                        try {
                            val req: SendRequest
                            val cachedAddOpReturn = addOpReturn

                            if(useTor)
                            {
                                if (coinAmt == getBalance(walletKit!!.wallet()) || coinAmt.isGreaterThan(getBalance(walletKit!!.wallet()))) {
                                    req = SendRequest.emptyWallet(parameters, recipientAddress, torProxy)

                                    /*
                                    Bitcoincashj requires emptying the wallet to only have a single output for some reason.
                                    So, we cache the original setting above, then set the real setting to false here.

                                    After doing the if(addOpReturn) check below, we restore it to its actual setting.
                                */
                                    addOpReturn = false
                                } else {
                                    req = SendRequest.to(parameters, recipientAddress, coinAmt, torProxy)
                                }
                            }
                            else
                            {
                                if (coinAmt == getBalance(walletKit!!.wallet()) || coinAmt.isGreaterThan(getBalance(walletKit!!.wallet()))) {
                                    req = SendRequest.emptyWallet(parameters, recipientAddress)

                                    /*
                                    Bitcoincashj requires emptying the wallet to only have a single output for some reason.
                                    So, we cache the original setting above, then set the real setting to false here.

                                    After doing the if(addOpReturn) check below, we restore it to its actual setting.
                                */
                                    addOpReturn = false
                                } else {
                                    req = SendRequest.to(parameters, recipientAddress, coinAmt)
                                }
                            }

                            req.ensureMinRequiredFee = false
                            req.aesKey = aesKey
                            req.feePerKb = Coin.valueOf(java.lang.Long.parseLong(1.toString() + "") * 1000L)

                            if (addOpReturn) {
                                val opReturnBytes = "Sent w/ crescent.cash!".toByteArray()
                                val script = ScriptBuilder.createOpReturnScript(opReturnBytes)

                                if (opReturnBytes.size <= Constants.MAX_OP_RETURN) {
                                    req.tx.addOutput(Coin.ZERO, script)
                                }
                            }

                            addOpReturn = cachedAddOpReturn

                            val tx = walletKit!!.wallet().sendCoinsOffline(req)
                            val txHexBytes = Hex.encode(tx.bitcoinSerialize())
                            val txHex = String(txHexBytes, StandardCharsets.UTF_8)
                            broadcastTxToPeers(walletKit!!.peerGroup(), tx)

                            if(!useTor) {
                                MainActivity.INSTANCE.netHelper.broadcastTransaction(txHex, "https://rest.bitcoin.com/v2/rawtransactions/sendRawTransaction")
                            }

                            MainActivity.INSTANCE.netHelper.broadcastTransaction(txHex, "https://rest.imaginary.cash/v2/rawtransactions/sendRawTransaction")
                        } catch (e: InsufficientMoneyException) {
                            e.printStackTrace()
                            MainActivity.INSTANCE.runOnUiThread { e.message?.let { uiHelper.showToastMessage(it) } }
                        } catch (e: Wallet.CouldNotAdjustDownwards) {
                            e.printStackTrace()
                            throwSendError("Not enough BCH for fee!")
                        } catch (e: Wallet.ExceededMaxTransactionSize) {
                            e.printStackTrace()
                            throwSendError("Transaction is too large!")
                        } catch (e: NullPointerException) {
                            e.printStackTrace()
                            throwSendError("Cash Account not found.")
                        }

                    }
                }
            }.start()
        } else if (!Address.isValidCashAddr(parameters, toAddress) || !Address.isValidLegacyAddress(parameters, toAddress)) {
            throwSendError("Invalid address!")
        }
    }

    private fun processBIP70(url: String)
    {
        try {
            val future: ListenableFuture<PaymentSession> = PaymentSession.createFromUrl(url)

            val session = future.get()
            if (session.isExpired) {
                throwSendError("Invoice expired!")
            }

            val req = session.sendRequest
            req.aesKey = aesKey
            wallet.completeTx(req)

            val ack = session.sendPayment(ImmutableList.of(req.tx), wallet.freshReceiveAddress(), null)
            if (ack != null) {
                Futures.addCallback<PaymentProtocol.Ack>(ack, object : FutureCallback<PaymentProtocol.Ack> {
                    override fun onSuccess(ack: PaymentProtocol.Ack?) {
                        wallet.commitTx(req.tx)
                        MainActivity.INSTANCE.runOnUiThread {
                            uiHelper.clearSend()
                        }
                    }

                    override fun onFailure(throwable: Throwable) {
                        throwSendError("An error occurred.")
                    }
                })
            }
        } catch (e: InsufficientMoneyException) {
            throwSendError("You do not have enough BCH!")
        }
    }

    fun throwSendError(message: String)
    {
        MainActivity.INSTANCE.runOnUiThread {
            uiHelper.showToastMessage(message)
            uiHelper.setSendButtonsActive()
        }
    }

    private fun setupWalletListeners(wallet: Wallet) {
        wallet.addCoinsReceivedEventListener { wallet1, tx, prevBalance, newBalance ->
            if (!uiHelper.isDisplayingDownload) {
                uiHelper.displayMyBalance(getBalance(wallet).toFriendlyString())

                if (tx.purpose == Transaction.Purpose.UNKNOWN)
                    uiHelper.showToastMessage("Received coins!")

                val v = MainActivity.INSTANCE.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

                v.vibrate(100)

                getSLPWallet().refreshBalance()

                uiHelper.refresh()

                playAudio(R.raw.coins_received)
            }
        }
        wallet.addCoinsSentEventListener { wallet12, tx, prevBalance, newBalance ->
            if (!uiHelper.isDisplayingDownload) {
                uiHelper.displayMyBalance(getBalance(wallet).toFriendlyString())
                uiHelper.showToastMessage("Sent coins!")
                getSLPWallet().refreshBalance()
                this.uiHelper.clearSend()
                uiHelper.refresh()

                playAudio(R.raw.send_coins)
            }
        }
    }

    fun sendToken(tokenId: String, amount: BigDecimal, address: String) {
        uiHelper.sendSLPBtn.isEnabled = false
        object : Thread() {
            override fun run() {

                val trySend = getSLPWallet().sendToken(tokenId, amount, address)

                try {
                    trySend.subscribe(object : SingleObserver<String> {
                        override fun onSubscribe(d: Disposable) {

                        }

                        override fun onSuccess(s: String) {
                            getSLPWallet().clearSendStatus()

                            MainActivity.INSTANCE.runOnUiThread {
                                uiHelper.showToastMessage("Sent!")
                                uiHelper.clearSend()
                                uiHelper.sendSLPWindow.visibility = View.GONE
                                uiHelper.refreshSLP()
                                uiHelper.refresh()
                            }

                            getSLPWallet().refreshBalance()
                        }

                        override fun onError(e: Throwable) {
                            getSLPWallet().clearSendStatus()
                            if(e.message != null)
                            {
                                MainActivity.INSTANCE.runOnUiThread { uiHelper.showToastMessage(e.message!!) }
                            }
                            else
                            {
                                MainActivity.INSTANCE.runOnUiThread { uiHelper.showToastMessage("Something went wrong.") }
                            }
                            uiHelper.setSendButtonsActive()
                            getSLPWallet().refreshBalance()
                        }
                    })
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }.start()
    }

    fun setupSLPWallet(seed: String?, hasSLPWallet: Boolean) {
        if (seed == null) {
            if (hasSLPWallet) {
                slpWallet = SLPWallet.getInstance(MainActivity.INSTANCE, if (Constants.IS_PRODUCTION) Network.MAIN else Network.TEST)
                MainActivity.INSTANCE.prefs.edit().putBoolean("hasSLPWallet", true).apply()
            } else {
                val seedFromWallet = wallet.keyChainSeed
                val mnemonicCode = seedFromWallet.mnemonicCode
                val recoverySeed = StringBuilder()

                assert(mnemonicCode != null)
                for (x in mnemonicCode!!.indices) {
                    recoverySeed.append(mnemonicCode[x]).append(if (x == mnemonicCode.size - 1) "" else " ")
                }

                slpWallet = SLPWallet.fromMnemonic(MainActivity.INSTANCE, if (Constants.IS_PRODUCTION) Network.MAIN else Network.TEST, recoverySeed.toString(), true)

                MainActivity.INSTANCE.prefs.edit().putBoolean("hasSLPWallet", true).apply()

                MainActivity.INSTANCE.runOnUiThread { uiHelper.refresh() }
            }
        } else {
            slpWallet = SLPWallet.fromMnemonic(MainActivity.INSTANCE, if (Constants.IS_PRODUCTION) Network.MAIN else Network.TEST, seed, true)
            MainActivity.INSTANCE.prefs.edit().putBoolean("hasSLPWallet", true).apply()
        }

        getSLPWallet().balance.observe(MainActivity.INSTANCE, Observer { data -> balances = data })
    }

    fun findBitcoinInSLPList(): BalanceInfo? {
        this.getSLPWallet().refreshBalance()

        for(coin in this.balances)
        {
            if(coin.tokenId == "")
            {
                return coin
            }
        }

        return null
    }

    fun getXPUB(): String {
        return serializedXpub
    }

    private fun broadcastTxToPeers(peerGroup: PeerGroup, tx: Transaction)
    {
        for(peer in peerGroup.connectedPeers)
        {
            peer.sendMessage(tx)
        }

        MainActivity.INSTANCE.walletHelper.getSLPWallet().refreshBalance()

        MainActivity.INSTANCE.runOnUiThread {
            uiHelper.displayMyBalance(MainActivity.INSTANCE.walletHelper.getBalance(MainActivity.INSTANCE.walletHelper.wallet).toFriendlyString())
            uiHelper.clearSend()
            uiHelper.refresh()
        }
    }

    private fun playAudio(audioFile: Int)
    {
        val mediaPlayer = MediaPlayer.create(MainActivity.INSTANCE, audioFile)
        mediaPlayer.start()
    }

    companion object {
        var addOpReturn: Boolean = false
        var encrypted: Boolean = false
        var useTor: Boolean = false

        val SCRYPT_PARAMETERS: Protos.ScryptParameters = Protos.ScryptParameters.newBuilder().setP(6).setR(8).setN(32768).setSalt(ByteString.copyFrom(KeyCrypterScrypt.randomSalt())).build()
    }
}
