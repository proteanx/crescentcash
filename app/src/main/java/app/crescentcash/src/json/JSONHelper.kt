package app.crescentcash.src.json

import app.crescentcash.src.MainActivity
import app.crescentcash.src.wallet.WalletHelper
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.lang.Exception

import java.net.URL
import java.nio.charset.Charset

class JSONHelper {

    fun getRegisterTxHash(jsonResponse: String): String {
        var hash = ""
        try {
            val json = JSONObject(jsonResponse)
            hash = json.getString("txid")
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        return hash
    }

    fun getJsonObject(url: String): JSONObject?
    {
        var `is`: InputStream? = null
        try {
            `is` = if(WalletHelper.useTor) URL(url).openConnection(MainActivity.INSTANCE.walletHelper.torProxy).getInputStream() else URL(url).openConnection().getInputStream()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return try {
            val rd = BufferedReader(InputStreamReader(`is`, Charset.forName("UTF-8")))
            val jsonText = readJSONFile(rd)
            JSONObject(jsonText)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        } finally {
            try {
                `is`?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    companion object {

        @Throws(IOException::class)
        fun readJSONFile(rd: Reader): String {
            val sb = StringBuilder()
            while (true) {
                val cp = rd.read()

                if(cp != -1)
                    sb.append(cp.toChar())
                else
                    break
            }
            return sb.toString()
        }
    }
}
