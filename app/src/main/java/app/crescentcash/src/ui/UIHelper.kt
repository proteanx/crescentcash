package app.crescentcash.src.ui

import android.app.Activity
import android.content.*
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.provider.ContactsContract
import androidx.annotation.UiThread

import android.text.Html
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.SimpleAdapter
import android.widget.Spinner
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.luminiasoft.ethereum.blockiesandroid.BlockiesIdenticon

import org.bitcoinj.core.Transaction
import org.bitcoinj.utils.MonetaryFormat
import org.bitcoinj.wallet.Wallet

import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat

import app.crescentcash.src.MainActivity
import app.crescentcash.src.R
import app.crescentcash.src.listener.RecipientTextListener
import app.crescentcash.src.qr.QRHelper
import app.crescentcash.src.utils.Constants
import app.crescentcash.src.wallet.SLPWalletHelper
import app.crescentcash.src.wallet.WalletHelper
import com.ncorti.slidetoact.SlideToActView
import org.bitcoinj.crypto.KeyCrypterScrypt
import java.net.InetSocketAddress
import java.net.Proxy
import java.net.URL
import java.text.DecimalFormatSymbols
import java.util.*

class UIHelper {
    private val createWalletBtn: Button
    private val restoreWalletBtn: Button
    var restore_wallet: FrameLayout
    var new_wallet: FrameLayout
    var newuser: FrameLayout
    private val qrScan: Button
    private val openKeys: ImageButton
    private val balance: TextView

    /////REGISTER SCREEN
    private val registerUserBtn: Button
    var handle: EditText
    /////REGISTER SCREEN

    /////RECOVERY SCREEN
    private val verifyUserBtn: Button
    var recoverySeed: EditText
    var handle2: EditText
    /////RECOVERY SCREEN


    /////WALLET SETTINGS SCREEN
    var walletSettings: FrameLayout
    private val btnShowSeed: Button
    private val xpub: TextView
    val autoSendAmount: TextView
    val autoSendDisplayUnits: TextView
    /////WALLET SETTINGS SCREEN

    /////RECEIVE BITCOIN STUFF
    private val copyBtcAddr: ImageView
    private val btcAddress: TextView
    private val copySlpAddr: ImageView
    private val slpAddress: TextView
    /////RECEIVE BITCOIN STUFF

    private val syncPct: TextView
    private val srlContent_AM: SwipeRefreshLayout
    private val srlHistory: SwipeRefreshLayout
    private val srlSLP: SwipeRefreshLayout
    private val myCashHandle: TextView

    private val tvRecipientAddress_AM: RecipientEditText
    val amountText: TextView
    val btnSend_AM: SlideToActView
    //private val closeSendBtn: Button
    private val ivCopy_AM: ImageView
    private val sendTab: Button
    private val receiveTab: Button
    //private val closeReceiveBtn: Button
    var sendWindow: FrameLayout
    var receiveWindow: FrameLayout
    var receiveWindowSLP: FrameLayout
    private val receiveTabSLP: Button

    //var historyWindow: FrameLayout
    var slpBalancesWindow: FrameLayout
    //private val historyTab: ImageButton
    //private val closeHistoryBtn: Button
    val setMaxCoins: Button
    private val txHistoryList: NonScrollListView
    private val slpList: NonScrollListView
    var txInfo: FrameLayout
    private val txInfoTV: TextView
    private val btnViewTx: Button
    var myEmoji: TextView
    var nightModeSwitch: Switch
    var sendType: Spinner
    var addOpReturnSwitch: Switch
    private val donateBtn: TextView
    private val viewSLPBtn: Button
    private val viewBCHBtn: Button
    //private val closeSLPBtn: Button
    var sendSLPWindow: FrameLayout
    val sendSLPBtn: SlideToActView
    private val setMaxSLP: Button
    private val slpUnit: TextView
    val bchBalanceSLP: TextView
    var slpAmount: TextView
    var slpRecipientAddress: TextView
    private val slp_qrScan: Button
    private val fiatBalTxt: TextView
    private val fiatBalTxtSend: TextView
    private val bchBalSend: TextView
    var showFiatSwitch: Switch
    private val contactsBtn: ImageButton
    private val copyXpubBtn: ImageView
    private val no_tx_text: TextView
    private val no_tx_text_slp: TextView
    var bitcoinWindow: FrameLayout
    private val tipBCHBtn: Button
    private val coinspiceBtn: Button
    private val sideshiftAIBtn: Button
    private val acceptbchBtn: Button
    private val toggleAddr: ImageButton
    var fiatType: Spinner
    var encryptWalletSwitch: Switch
    var encryptScreen: FrameLayout
    val encryptBtn: Button
    var encryptionPassword: EditText
    var encryptionPwdConfirm: EditText
    var unlockScreen: FrameLayout
    var unlockPassword: EditText
    val unlockBtn: Button
    var useTorSwitch: Switch

    var emojis = intArrayOf(128123, 128018, 128021, 128008, 128014, 128004, 128022, 128016, 128042, 128024, 128000, 128007, 128063, 129415, 128019, 128039, 129414, 129417, 128034, 128013, 128031, 128025, 128012, 129419, 128029, 128030, 128375, 127803, 127794, 127796, 127797, 127809, 127808, 127815, 127817, 127819, 127820, 127822, 127826, 127827, 129373, 129381, 129365, 127805, 127798, 127812, 129472, 129370, 129408, 127850, 127874, 127853, 127968, 128663, 128690, 9973, 9992, 128641, 128640, 8986, 9728, 11088, 127752, 9730, 127880, 127872, 9917, 9824, 9829, 9830, 9827, 128083, 128081, 127913, 128276, 127925, 127908, 127911, 127928, 127930, 129345, 128269, 128367, 128161, 128214, 9993, 128230, 9999, 128188, 128203, 9986, 128273, 128274, 128296, 128295, 9878, 9775, 128681, 128099, 127838)

    private var currentAddrView = true

    val recipient: String
        get() = tvRecipientAddress_AM.text.toString().trim { it <= ' ' }

    val isDisplayingDownload: Boolean
        get() = MainActivity.INSTANCE.walletHelper.downloading

    val amount: String
        get() = amountText.text.toString()

    init {
        val mainActivity = MainActivity.INSTANCE

        createWalletBtn = mainActivity.findViewById(R.id.createWalletBtn)
        restoreWalletBtn = mainActivity.findViewById(R.id.restoreWalletBtn)
        restore_wallet = mainActivity.findViewById(R.id.restore_wallet)
        new_wallet = mainActivity.findViewById(R.id.new_wallet)
        newuser = mainActivity.findViewById(R.id.newuser)
        qrScan = mainActivity.findViewById(R.id.qrScan)
        openKeys = mainActivity.findViewById(R.id.openKeys)
        balance = mainActivity.findViewById(R.id.balance)
        registerUserBtn = mainActivity.findViewById(R.id.registerUserBtn)
        handle = mainActivity.findViewById(R.id.handle)
        verifyUserBtn = mainActivity.findViewById(R.id.verifyUserBtn)
        recoverySeed = mainActivity.findViewById(R.id.recoverySeed)
        handle2 = mainActivity.findViewById(R.id.handle2)
        walletSettings = mainActivity.findViewById(R.id.walletSettings)
        btnShowSeed = mainActivity.findViewById(R.id.btnShowSeed)
        xpub = mainActivity.findViewById(R.id.xpub)
        copyBtcAddr = mainActivity.findViewById(R.id.copyBtcAddr)
        btcAddress = mainActivity.findViewById(R.id.btcAddress)
        srlContent_AM = mainActivity.findViewById(R.id.srlContent_AM)
        srlHistory = mainActivity.findViewById(R.id.srlHistory)
        myCashHandle = mainActivity.findViewById(R.id.myCashHandle)
        tvRecipientAddress_AM = mainActivity.findViewById(R.id.tvRecipientAddress_AM)
        amountText = mainActivity.findViewById(R.id.etAmount_AM)
        btnSend_AM = mainActivity.findViewById(R.id.btnSend_AM)
        //closeSendBtn = mainActivity.findViewById(R.id.closeSendBtn)
        ivCopy_AM = mainActivity.findViewById(R.id.ivCopy_AM)
        sendTab = mainActivity.findViewById(R.id.sendTab)
        receiveTab = mainActivity.findViewById(R.id.receiveTab)
        //closeReceiveBtn = mainActivity.findViewById(R.id.closeReceiveBtn)
        sendWindow = mainActivity.findViewById(R.id.sendWindow)
        receiveWindow = mainActivity.findViewById(R.id.receiveWindow)
        //historyWindow = mainActivity.findViewById(R.id.historyWindow)
        //historyTab = mainActivity.findViewById(R.id.historyTab)
        //closeHistoryBtn = mainActivity.findViewById(R.id.closeHistoryBtn)
        setMaxCoins = mainActivity.findViewById(R.id.setMaxCoins)
        txHistoryList = mainActivity.findViewById(R.id.txHistoryList)
        txInfo = mainActivity.findViewById(R.id.txInfo)
        txInfoTV = mainActivity.findViewById(R.id.txInfoTV)
        btnViewTx = mainActivity.findViewById(R.id.btnViewTx)
        syncPct = mainActivity.findViewById(R.id.syncPct)
        myEmoji = mainActivity.findViewById(R.id.myEmoji)
        nightModeSwitch = mainActivity.findViewById(R.id.nightModeSwitch)
        sendType = mainActivity.findViewById(R.id.sendType)
        addOpReturnSwitch = mainActivity.findViewById(R.id.addOpReturnSwitch)
        donateBtn = mainActivity.findViewById(R.id.donateBtn)
        srlSLP = mainActivity.findViewById(R.id.srlSLP)
        slpBalancesWindow = mainActivity.findViewById(R.id.slpBalancesWindow)
        slpList = mainActivity.findViewById(R.id.slpList)
        viewSLPBtn = mainActivity.findViewById(R.id.viewSLPBtn)
        viewBCHBtn = mainActivity.findViewById(R.id.viewBCHBtn)
        //closeSLPBtn = mainActivity.findViewById(R.id.closeSLPBtn)
        sendSLPWindow = mainActivity.findViewById(R.id.sendSLPWindow)
        sendSLPBtn = mainActivity.findViewById(R.id.sendSLPBtn)
        setMaxSLP = mainActivity.findViewById(R.id.setMaxSLP)
        slpUnit = mainActivity.findViewById(R.id.slpUnit)
        slpAmount = mainActivity.findViewById(R.id.slpAmount)
        slpRecipientAddress = mainActivity.findViewById(R.id.slpRecipientAddress)
        slp_qrScan = mainActivity.findViewById(R.id.slp_qrScan)
        fiatBalTxt = mainActivity.findViewById(R.id.fiatBalTxt)
        fiatBalTxtSend = mainActivity.findViewById(R.id.fiatBalTxtSend)
        bchBalSend = mainActivity.findViewById(R.id.bchBalSend)
        showFiatSwitch = mainActivity.findViewById(R.id.showFiatSwitch)
        contactsBtn = mainActivity.findViewById(R.id.contactsBtn)
        copyXpubBtn = mainActivity.findViewById(R.id.copyXpubBtn)
        no_tx_text = mainActivity.findViewById(R.id.no_tx_text)
        no_tx_text_slp = mainActivity.findViewById(R.id.no_tx_text_slp)
        bitcoinWindow = mainActivity.findViewById(R.id.bitcoinWindow)
        tipBCHBtn = mainActivity.findViewById(R.id.tipBCHBtn)
        coinspiceBtn = mainActivity.findViewById(R.id.coinspiceBtn)
        sideshiftAIBtn = mainActivity.findViewById(R.id.sideshiftAIBtn)
        acceptbchBtn = mainActivity.findViewById(R.id.acceptbchBtn)
        autoSendAmount = mainActivity.findViewById(R.id.autoSendAmt)
        autoSendDisplayUnits = mainActivity.findViewById(R.id.autoSendDisplayUnits)
        receiveWindowSLP = mainActivity.findViewById(R.id.receiveWindowSLP)
        receiveTabSLP = mainActivity.findViewById(R.id.receiveTabSLP)
        copySlpAddr = mainActivity.findViewById(R.id.copySlpAddr)
        slpAddress = mainActivity.findViewById(R.id.slpAddress)
        fiatType = mainActivity.findViewById(R.id.fiatType)
        bchBalanceSLP = mainActivity.findViewById(R.id.bchBalanceSLP)
        toggleAddr = mainActivity.findViewById(R.id.toggleAddr)
        encryptWalletSwitch = mainActivity.findViewById(R.id.encryptWalletSwitch)
        encryptScreen = mainActivity.findViewById(R.id.encryptScreen)
        encryptBtn = mainActivity.findViewById(R.id.encryptBtn)
        encryptionPassword = mainActivity.findViewById(R.id.encryptionPassword)
        encryptionPwdConfirm = mainActivity.findViewById(R.id.encryptionPwdConfirm)
        unlockScreen = mainActivity.findViewById(R.id.unlockScreen)
        unlockPassword = mainActivity.findViewById(R.id.unlockPassword)
        unlockBtn = mainActivity.findViewById(R.id.unlockBtn)
        useTorSwitch = mainActivity.findViewById(R.id.useTorSwitch)

        this.initListeners()
    }

    private fun initListeners() {
        this.restoreWalletBtn.setOnClickListener { displayRestore() }
        this.createWalletBtn.setOnClickListener { displayNewWallet() }
        this.srlContent_AM.setOnRefreshListener { this.refresh() }
        this.srlHistory.setOnRefreshListener { this.refresh() }
        this.srlSLP.setOnRefreshListener { this.refreshSLP() }

        val slideBCHListener: SlideToActView.OnSlideCompleteListener = object : SlideToActView.OnSlideCompleteListener {
            override fun onSlideComplete(view: SlideToActView) {
                MainActivity.INSTANCE.walletHelper.send()
            }
        }

        this.btnSend_AM.onSlideCompleteListener = slideBCHListener

        this.ivCopy_AM.setOnClickListener {
            val cashAccount = MainActivity.INSTANCE.prefs.getString("cashAccount", "")
            val clip = ClipData.newPlainText("My Cash Acct", cashAccount + "; " + myEmoji.text)
            val clipboard = MainActivity.INSTANCE.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            clipboard.primaryClip = clip
            Toast.makeText(MainActivity.INSTANCE, "Copied", Toast.LENGTH_SHORT).show()
        }

        this.copyBtcAddr.setOnClickListener {
            val clip: ClipData = ClipData.newPlainText("My BCH address", MainActivity.INSTANCE.walletHelper.parameters.cashAddrPrefix + ":" + btcAddress.text.toString())
            val clipboard = MainActivity.INSTANCE.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            clipboard.primaryClip = clip
            Toast.makeText(MainActivity.INSTANCE, "Copied", Toast.LENGTH_SHORT).show()
        }

        this.copySlpAddr.setOnClickListener {
            val clip: ClipData = if(currentAddrView) ClipData.newPlainText("My SLP address", "simpleledger:" + slpAddress.text.toString()) else ClipData.newPlainText("My BCH SLP address", "bitcoincash:" + slpAddress.text.toString())
            val clipboard = MainActivity.INSTANCE.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            clipboard.primaryClip = clip
            Toast.makeText(MainActivity.INSTANCE, "Copied", Toast.LENGTH_SHORT).show()
        }

        this.copyXpubBtn.setOnClickListener {
            val clip: ClipData = ClipData.newPlainText("My xpub", MainActivity.INSTANCE.walletHelper.getXPUB())

            val clipboard = MainActivity.INSTANCE.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            clipboard.primaryClip = clip
            Toast.makeText(MainActivity.INSTANCE, "Copied", Toast.LENGTH_SHORT).show()
        }

        this.donateBtn.setOnClickListener { this.displayRecipientAddress("bitcoincash:qptnypuugy29lttleggl7l0vpls0vg295q9nsavw6g") }

        this.sendTab.setOnClickListener {displaySendWindow() }
        this.receiveTab.setOnClickListener { displayReceive() }
        this.receiveTabSLP.setOnClickListener { displayReceiveSLP() }
        this.qrScan.setOnClickListener { clickScanQR() }
        this.openKeys.setOnClickListener { displayWalletKeys() }

        txHistoryList.setOnItemClickListener { parent, view, position, id ->
            val tx = MainActivity.INSTANCE.txList[position]
            displayTxWindow(tx)
        }

        this.nightModeSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (MainActivity.INSTANCE.started) {
                nightModeEnabled = isChecked
                MainActivity.INSTANCE.prefs.edit().putBoolean("nightMode", nightModeEnabled).apply()
            }
        }

        this.addOpReturnSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (MainActivity.INSTANCE.started) {
                WalletHelper.addOpReturn = isChecked
                MainActivity.INSTANCE.prefs.edit().putBoolean("addOpReturn", WalletHelper.addOpReturn).apply()
            }
        }

        this.showFiatSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (MainActivity.INSTANCE.started) {
                showFiat = isChecked
                MainActivity.INSTANCE.prefs.edit().putBoolean("showFiat", showFiat).apply()
                refresh()
            }
        }

        this.encryptWalletSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (MainActivity.INSTANCE.started) {
                if(isChecked)
                {
                    this.displayEncryptionSetup()
                }
                else
                {
                    if(MainActivity.INSTANCE.walletHelper.aesKey != null) {
                        MainActivity.INSTANCE.walletHelper.wallet.decrypt(MainActivity.INSTANCE.walletHelper.aesKey)
                        MainActivity.INSTANCE.walletHelper.aesKey = null
                    }
                }

                WalletHelper.encrypted = isChecked
                MainActivity.INSTANCE.prefs.edit().putBoolean("useEncryption", WalletHelper.encrypted).apply()
            }
        }

        this.useTorSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (MainActivity.INSTANCE.started) {
                WalletHelper.useTor = isChecked

                if(isChecked)
                {
                    MainActivity.INSTANCE.walletHelper.torProxy = Proxy(Proxy.Type.SOCKS, InetSocketAddress("127.0.0.1", 9050))
                }

                MainActivity.INSTANCE.prefs.edit().putBoolean("useTor", WalletHelper.useTor).apply()
            }
        }

        this.toggleAddr.setOnClickListener { this.toggleAddress() }

        viewBCHBtn.setOnClickListener {
            bitcoinWindow.visibility = View.VISIBLE
            refresh()
        }
        viewSLPBtn.setOnClickListener {
            MainActivity.INSTANCE.walletHelper.getSLPWallet().refreshBalance()
            refresh()
            setSLPList()
            slpBalancesWindow.visibility = View.VISIBLE
        }

        slpList.setOnItemClickListener { parent, view, position, id ->
            val walletHelper = MainActivity.INSTANCE.walletHelper
            walletHelper.currentTokenPosition = position
            walletHelper.currentTokenId = walletHelper.tokenListInfo[walletHelper.currentTokenPosition].tokenId

            sendSLPWindow.visibility = View.VISIBLE
            slpUnit.text = walletHelper.tokenListInfo[walletHelper.currentTokenPosition].ticker
        }

        setMaxSLP.setOnClickListener {
            val walletHelper = MainActivity.INSTANCE.walletHelper
            slpAmount.text = walletHelper.tokenListInfo[walletHelper.currentTokenPosition].amount.toPlainString()
        }

        val slideSLPListener: SlideToActView.OnSlideCompleteListener = object : SlideToActView.OnSlideCompleteListener {
            override fun onSlideComplete(view: SlideToActView) {
                if (!TextUtils.isEmpty(slpRecipientAddress.text) && !TextUtils.isEmpty(slpAmount.text)) {
                    if (!slpRecipientAddress.text.toString().contains("#")) {
                        val tokenId = MainActivity.INSTANCE.walletHelper.currentTokenId
                        val tokenPos = MainActivity.INSTANCE.walletHelper.currentTokenPosition
                        val tokenInfo = MainActivity.INSTANCE.walletHelper.tokenListInfo[tokenPos]
                        val amt = BigDecimal(java.lang.Double.parseDouble(slpAmount.text.toString())).setScale(tokenInfo.decimals!!, RoundingMode.HALF_UP)
                        println(amt.toPlainString())
                        MainActivity.INSTANCE.walletHelper.sendToken(tokenId, amt, slpRecipientAddress.text.toString())
                    } else {
                        MainActivity.INSTANCE.runOnUiThread { showToastMessage("SLP CashAccts are not supported.") }
                    }
                }
            }
        }

        sendSLPBtn.onSlideCompleteListener = slideSLPListener

        val listener: RecipientTextListener = object : RecipientTextListener {
            override fun onUpdate() {
                val clipboard = MainActivity.INSTANCE.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val clipboardText = clipboard.text.toString()

                MainActivity.INSTANCE.walletHelper.processScanOrPaste(clipboardText)
            }
        }

        tvRecipientAddress_AM.addListener(listener)

        this.slp_qrScan.setOnClickListener { clickScanQR() }

        this.setMaxCoins.setOnClickListener { setMaxCoins() }
        this.contactsBtn.setOnClickListener { showContactSelectionScreen() }

        this.tipBCHBtn.setOnClickListener {
            val url = "https://tipbitcoin.cash"
            val uri = Uri.parse(url)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            MainActivity.INSTANCE.startActivity(intent)
        }
        this.coinspiceBtn.setOnClickListener {
            val url = "https://coinspice.io"
            val uri = Uri.parse(url)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            MainActivity.INSTANCE.startActivity(intent)
        }
        this.sideshiftAIBtn.setOnClickListener {
            val url = "https://sideshift.ai/a/mmG1iwJRO"
            val uri = Uri.parse(url)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            MainActivity.INSTANCE.startActivity(intent)
        }
        this.acceptbchBtn.setOnClickListener {
            val url = "https://acceptbitcoin.cash"
            val uri = Uri.parse(url)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            MainActivity.INSTANCE.startActivity(intent)
        }
    }

    fun displayEncryptionSetup()
    {
        this.encryptScreen.visibility = View.VISIBLE
        this.encryptBtn.setOnClickListener {
            if(!TextUtils.isEmpty(encryptionPassword.text) && !TextUtils.isEmpty(encryptionPwdConfirm.text))
            {
                if(encryptionPassword.text.toString() == encryptionPwdConfirm.text.toString())
                {
                    this.showToastMessage("Encrypting...")
                    val password = encryptionPassword.text.toString()

                    val scrypt = KeyCrypterScrypt(WalletHelper.SCRYPT_PARAMETERS)
                    val key = scrypt.deriveKey(password)
                    MainActivity.INSTANCE.walletHelper.wallet.encrypt(scrypt, key)
                    this.showToastMessage("Encrypted wallet!")
                    MainActivity.INSTANCE.walletHelper.aesKey = key
                    this.encryptScreen.visibility = View.GONE
                }
                else
                {
                    this.showToastMessage("Passwords do not match.")
                }
            }
            else
            {
                this.showToastMessage("Please enter a password.")
            }
        }
    }

    fun displayUnlockScreen()
    {
        this.unlockScreen.visibility = View.VISIBLE

        this.unlockBtn.setOnClickListener {
            if(!TextUtils.isEmpty(unlockPassword.text))
            {
                val password = unlockPassword.text.toString()
                val scrypt = MainActivity.INSTANCE.walletHelper.wallet.keyCrypter
                val key = scrypt!!.deriveKey(password)
                if(MainActivity.INSTANCE.walletHelper.wallet.checkAESKey(key))
                {
                    MainActivity.INSTANCE.walletHelper.aesKey = key
                    this.showToastMessage("Decrypted!")
                    this.unlockScreen.visibility = View.GONE
                }
                else
                {
                    this.showToastMessage("Invalid password.")
                }
            }
            else
            {
                this.showToastMessage("Please enter your password.")
            }
        }
    }

    private fun toggleAddress() {
        this.currentAddrView = !this.currentAddrView

        if (currentAddrView)
            this.displayCashAccountSLP(MainActivity.INSTANCE.walletHelper.getSLPWallet().slpAddress)
        else
            this.displayCashAccountSLP(MainActivity.INSTANCE.walletHelper.getSLPWallet().bchAddress)
    }

    private fun displaySendWindow()
    {
        sendWindow.visibility = View.VISIBLE
        receiveWindow.visibility = View.GONE

        val items = arrayOf(MainActivity.INSTANCE.walletHelper.displayUnits, fiat)
        val adapter = ArrayAdapter(MainActivity.INSTANCE, R.layout.spinner_item, items)
        sendType.adapter = adapter

        when {
            MainActivity.INSTANCE.walletHelper.sendType == MainActivity.INSTANCE.walletHelper.displayUnits -> sendType.setSelection(0)
            MainActivity.INSTANCE.walletHelper.sendType == fiat -> sendType.setSelection(1)
        }

        sendType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, v: View, position: Int, id: Long) {

                when (position) {
                    0 -> MainActivity.INSTANCE.walletHelper.sendType = MainActivity.INSTANCE.walletHelper.displayUnits
                    1 -> MainActivity.INSTANCE.walletHelper.sendType = fiat
                }

                MainActivity.INSTANCE.prefs.edit().putString("sendType", MainActivity.INSTANCE.walletHelper.sendType).apply()
                println(MainActivity.INSTANCE.walletHelper.sendType)
                refresh()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
    }

    private fun displayTxWindow(tx: Transaction) {
        val symbols = DecimalFormatSymbols(Locale.US)
        var decimalFormatter: DecimalFormat?
        var receivedValueStr = ""

        when {
            MainActivity.INSTANCE.walletHelper.displayUnits == MonetaryFormat.CODE_BTC -> {
                decimalFormatter = DecimalFormat("#,###.########", symbols)
                receivedValueStr = MonetaryFormat.BTC.format(tx.getValue(MainActivity.INSTANCE.walletHelper.wallet)).toString()
                receivedValueStr = receivedValueStr.replace(MainActivity.INSTANCE.walletHelper.displayUnits + " ", "")
                receivedValueStr = decimalFormatter.format(java.lang.Double.parseDouble(receivedValueStr))
            }
            MainActivity.INSTANCE.walletHelper.displayUnits == MonetaryFormat.CODE_MBTC -> {
                decimalFormatter = DecimalFormat("#,###.#####", symbols)
                receivedValueStr = MonetaryFormat.MBTC.format(tx.getValue(MainActivity.INSTANCE.walletHelper.wallet)).toString()
                receivedValueStr = receivedValueStr.replace(MainActivity.INSTANCE.walletHelper.displayUnits + " ", "")
                receivedValueStr = decimalFormatter.format(java.lang.Double.parseDouble(receivedValueStr))
            }
            MainActivity.INSTANCE.walletHelper.displayUnits == MonetaryFormat.CODE_UBTC -> {
                decimalFormatter = DecimalFormat("#,###.##", symbols)
                receivedValueStr = MonetaryFormat.UBTC.format(tx.getValue(MainActivity.INSTANCE.walletHelper.wallet)).toString()
                receivedValueStr = receivedValueStr.replace(MainActivity.INSTANCE.walletHelper.displayUnits + " ", "")
                receivedValueStr = decimalFormatter.format(java.lang.Double.parseDouble(receivedValueStr))
            }
            MainActivity.INSTANCE.walletHelper.displayUnits == "sats" -> {
                decimalFormatter = DecimalFormat("#,###", symbols)
                val amt = java.lang.Double.parseDouble(tx.getValue(MainActivity.INSTANCE.walletHelper.wallet).toPlainString())
                val formatted = amt * 100000000

                val formattedStr = decimalFormatter.format(formatted)
                receivedValueStr = formattedStr
            }
        }

        receivedValueStr = receivedValueStr.replace(MainActivity.INSTANCE.walletHelper.displayUnits + " ", "")
        receivedValueStr = receivedValueStr.replace("-", "")
        val feeStr: String

        if (tx.fee != null) {
            decimalFormatter = DecimalFormat("#,###.########", symbols)
            var feeValueStr = MonetaryFormat.BTC.format(tx.fee).toString()
            feeValueStr = feeValueStr.replace("BCH ", "")
            val fee = java.lang.Float.parseFloat(feeValueStr)
            feeStr = decimalFormatter.format(fee.toDouble())
        } else {
            feeStr = "n/a"
        }

        val txConfirmations = tx.confidence
        val txConfirms = "" + txConfirmations.depthInBlocks
        val txDate = tx.updateTime.toString() + ""
        val txHash = tx.hashAsString
        txInfo.visibility = View.VISIBLE
        txInfoTV.text = Html.fromHtml("<b>" + MainActivity.INSTANCE.walletHelper.displayUnits + " Transferred:</b> " + receivedValueStr + "<br> <b>Fee:</b> " + feeStr + "<br> <b>Date:</b> " + txDate + "<br> <b>Confirmations:</b> " + txConfirms)
        btnViewTx.setOnClickListener { v ->
            val url = if (Constants.IS_PRODUCTION) "https://explorer.bitcoin.com/bch/tx/" else "https://explorer.bitcoin.com/tbch/tx/"
            val uri = Uri.parse(url + txHash) // missing 'http://' will cause crashed
            val intent = Intent(Intent.ACTION_VIEW, uri)
            MainActivity.INSTANCE.startActivity(intent)
        }
    }

    fun displayCashAccount(cashAccount: String) {
        myCashHandle.text = cashAccount

        if (srlContent_AM.isRefreshing) srlContent_AM.isRefreshing = false
    }

    private fun displayCashAccount(wallet: Wallet, cashAccount: String?) {
        myCashHandle.text = cashAccount

        val bchAddress: String

        if (MainActivity.INSTANCE.walletHelper.walletKit != null) {
            bchAddress = MainActivity.INSTANCE.walletHelper.wallet.currentReceiveAddress().toString()
            btcAddress.text = bchAddress.replace(MainActivity.INSTANCE.walletHelper.parameters.cashAddrPrefix + ":", "")
            generateQR(bchAddress, R.id.btcQR, false)
        } else {
            bchAddress = "Loading..."
            btcAddress.text = bchAddress
        }

        if (srlContent_AM.isRefreshing) srlContent_AM.isRefreshing = false
    }

    private fun displayCashAccountSLP(address: String) {
        if (MainActivity.INSTANCE.walletHelper.walletKit != null) {
            slpAddress.text = address.replace("simpleledger:", "").replace(MainActivity.INSTANCE.walletHelper.parameters.cashAddrPrefix + ":", "")

            if(currentAddrView)
                generateQR(address, R.id.slpQR, true)
            else
                generateQR(address, R.id.slpQR, false)
        } else {
            slpAddress.text = "Loading..."
        }

        if (srlContent_AM.isRefreshing) srlContent_AM.isRefreshing = false
    }

    private fun displayEmoji(emoji: String) {
        myEmoji.text = emoji

        if (srlContent_AM.isRefreshing) srlContent_AM.isRefreshing = false
    }


    private fun displayNewWallet() {
        restore_wallet.visibility = View.GONE
        new_wallet.visibility = View.VISIBLE
        newuser.visibility = View.GONE

        registerUserBtn.setOnClickListener { MainActivity.INSTANCE.netHelper.prepareWalletForRegistration() }
    }

    private fun displayWalletKeys() {
        if(MainActivity.INSTANCE.walletHelper.walletKit!!.isRunning) {
            val decimalFormatter = DecimalFormat("#.########", DecimalFormatSymbols(Locale.US))
            this.autoSendAmount.text = decimalFormatter.format(MainActivity.INSTANCE.walletHelper.maximumAutomaticSend)
            this.autoSendDisplayUnits.text = MainActivity.INSTANCE.walletHelper.displayUnits

            if(MainActivity.INSTANCE.walletHelper.aesKey != null) {
                var seed = MainActivity.INSTANCE.walletHelper.wallet.keyChainSeed
                seed = seed.decrypt(MainActivity.INSTANCE.walletHelper.wallet.keyCrypter, "", MainActivity.INSTANCE.walletHelper.aesKey)
                val mnemonicCode = seed.mnemonicCode
                val recoverySeedStr = StringBuilder()

                assert(mnemonicCode != null)
                for (x in mnemonicCode!!.indices) {
                    recoverySeedStr.append(mnemonicCode[x]).append(" ")
                }

                this.setWalletInfo(recoverySeedStr.toString())
            }
            else
            {
                val seed = MainActivity.INSTANCE.walletHelper.wallet.keyChainSeed
                if(seed.isEncrypted)
                {
                    this.showToastMessage("Wallet is encrypted!")
                }
                else
                {
                    val mnemonicCode = seed.mnemonicCode
                    val recoverySeedStr = StringBuilder()

                    assert(mnemonicCode != null)
                    for (x in mnemonicCode!!.indices) {
                        recoverySeedStr.append(mnemonicCode[x]).append(" ")
                    }

                    this.setWalletInfo(recoverySeedStr.toString())
                }
            }

            val dropdown = MainActivity.INSTANCE.findViewById<Spinner>(R.id.unitDropdown)
            val unitItems = arrayOf(MonetaryFormat.CODE_BTC, MonetaryFormat.CODE_MBTC, MonetaryFormat.CODE_UBTC, "sats")
            val unitAdapter = ArrayAdapter(MainActivity.INSTANCE, R.layout.spinner_item, unitItems)
            dropdown.adapter = unitAdapter

            when {
                MainActivity.INSTANCE.walletHelper.displayUnits == MonetaryFormat.CODE_BTC -> dropdown.setSelection(0)
                MainActivity.INSTANCE.walletHelper.displayUnits == MonetaryFormat.CODE_MBTC -> dropdown.setSelection(1)
                MainActivity.INSTANCE.walletHelper.displayUnits == MonetaryFormat.CODE_UBTC -> dropdown.setSelection(2)
                MainActivity.INSTANCE.walletHelper.displayUnits == "sats" -> dropdown.setSelection(3)
            }

            dropdown.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, v: View, position: Int, id: Long) {
                    val previous = MainActivity.INSTANCE.prefs.getString("displayUnit", "BCH")

                    when (position) {
                        0 -> MainActivity.INSTANCE.walletHelper.displayUnits = MonetaryFormat.CODE_BTC
                        1 -> MainActivity.INSTANCE.walletHelper.displayUnits = MonetaryFormat.CODE_MBTC
                        2 -> MainActivity.INSTANCE.walletHelper.displayUnits = MonetaryFormat.CODE_UBTC
                        3 -> MainActivity.INSTANCE.walletHelper.displayUnits = "sats"
                    }

                    if(MainActivity.INSTANCE.walletHelper.sendType == previous)
                        MainActivity.INSTANCE.walletHelper.sendType = MainActivity.INSTANCE.walletHelper.displayUnits

                    autoSendDisplayUnits.text = MainActivity.INSTANCE.walletHelper.displayUnits

                    val editor = MainActivity.INSTANCE.prefs.edit()
                    editor.putString("displayUnit", MainActivity.INSTANCE.walletHelper.displayUnits)
                    editor.putString("sendType", MainActivity.INSTANCE.walletHelper.sendType)
                    editor.apply()
                    println(MainActivity.INSTANCE.walletHelper.displayUnits)
                    refresh()
                }

                override fun onNothingSelected(parent: AdapterView<*>) {

                }
            }

            val fiatItems = arrayOf("USD", "EUR", "AUD")
            val fiatAdapter = ArrayAdapter(MainActivity.INSTANCE, R.layout.spinner_item, fiatItems)
            fiatType.adapter = fiatAdapter

            when (fiat) {
                "USD" -> fiatType.setSelection(0)
                "EUR" -> fiatType.setSelection(1)
                "AUD" -> fiatType.setSelection(2)
            }

            fiatType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, v: View, position: Int, id: Long) {

                    when (position) {
                        0 -> fiat = "USD"
                        1 -> fiat = "EUR"
                        2 -> fiat = "AUD"
                    }

                    MainActivity.INSTANCE.prefs.edit().putString("fiat", fiat).apply()
                    println(fiat)
                    refresh()
                }

                override fun onNothingSelected(parent: AdapterView<*>) {

                }
            }

            walletSettings.visibility = View.VISIBLE
        }
    }

    private fun displayReceive() {
        val cashAccount = MainActivity.INSTANCE.prefs.getString("cashAccount", "")
        displayCashAccount(MainActivity.INSTANCE.walletHelper.wallet, cashAccount)
        sendWindow.visibility = View.GONE
        receiveWindow.visibility = View.VISIBLE
    }

    private fun displayReceiveSLP() {
        if (currentAddrView)
            this.displayCashAccountSLP(MainActivity.INSTANCE.walletHelper.getSLPWallet().slpAddress)
        else
            this.displayCashAccountSLP(MainActivity.INSTANCE.walletHelper.getSLPWallet().bchAddress)
        sendWindow.visibility = View.GONE
        receiveWindowSLP.visibility = View.VISIBLE
    }

    private fun setMaxCoins() {
        MainActivity.INSTANCE.walletHelper.sendType = MainActivity.INSTANCE.walletHelper.displayUnits
        sendType.setSelection(0)
        MainActivity.INSTANCE.prefs.edit().putString("sendType", MainActivity.INSTANCE.walletHelper.sendType).apply()

        var coins = balance.text.toString().replace(" " + MainActivity.INSTANCE.walletHelper.displayUnits, "")
        coins = coins.replace(",", "")
        println("Setting...")
        MainActivity.INSTANCE.runOnUiThread { amountText.text = coins }
    }

    private fun displayRestore() {
        MainActivity.isNewUser = false
        restore_wallet.visibility = View.VISIBLE
        new_wallet.visibility = View.GONE
        newuser.visibility = View.GONE
        verifyUserBtn.setOnClickListener { MainActivity.INSTANCE.netHelper.prepareWalletForVerification() }
    }

    @UiThread
    fun displayDownloadContent(status: Boolean) {
        MainActivity.INSTANCE.walletHelper.downloading = status

        if (!status) {
            syncPct.text = ""
        }

    }

    private fun setArrayAdapter(wallet: Wallet) {
        setListViewShit(wallet)

        if (srlHistory.isRefreshing) srlHistory.isRefreshing = false
    }

    fun clearSend()
    {
        this.setSendButtonsActive()
        this.displayRecipientAddress(null)
        this.clearAmount()
        this.slpRecipientAddress.text = null
        this.slpAmount.text = null
        MainActivity.INSTANCE.walletHelper.currentTokenId = ""
        MainActivity.INSTANCE.walletHelper.currentTokenPosition = 0
        setSLPList()
    }

    fun setSendButtonsActive()
    {
        MainActivity.INSTANCE.runOnUiThread {
            this.sendSLPBtn.isEnabled = true
            this.btnSend_AM.isEnabled = true
            this.sendSLPBtn.resetSlider()
            this.btnSend_AM.resetSlider()
        }
    }

    fun refreshSLP() {
        setSLPList()

        if (srlSLP.isRefreshing) srlSLP.isRefreshing = false
    }

    private fun setSLPList() {
        val walletHelper = MainActivity.INSTANCE.walletHelper
        walletHelper.getSLPWallet().refreshBalance()
        if (walletHelper.balances.isNotEmpty()) {
            walletHelper.tokenList = ArrayList()
            walletHelper.tokenListInfo = ArrayList()
            val amtOfTokens = walletHelper.balances.size

            if(amtOfTokens >= 2) {
                srlSLP.visibility = View.VISIBLE
                no_tx_text_slp.visibility = View.GONE

                for (x in 0 until amtOfTokens) {
                    /*
                    SLP SDK includes BCH in the balance list, with a tokenHash of "" so we check shortly after gathering all of the data if the hash does not equal "" before adding
                     */
                    val datum = HashMap<String, String>()
                    val tokenName = walletHelper.balances[x].name
                    val tokenTicker = walletHelper.balances[x].ticker
                    val tokenHash = walletHelper.balances[x].tokenId
                    val balance = walletHelper.balances[x].amount.toPlainString()
                    val amt = BigDecimal(java.lang.Double.parseDouble(balance)).setScale(walletHelper.balances[x].decimals!!, RoundingMode.HALF_UP)

                    datum["token"] = tokenName!!
                    datum["tokenHash"] = tokenHash
                    datum["tokenTicker"] = tokenTicker!!
                    datum["balance"] = amt.toPlainString()

                    if(tokenHash != "") {
                        walletHelper.tokenList.add(datum)
                        walletHelper.tokenListInfo.add(walletHelper.balances[x])
                    }
                }

                val itemsAdapter = object : SimpleAdapter(MainActivity.INSTANCE, walletHelper.tokenList, R.layout.list_view_activity, null, null) {
                    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                        // Get the Item from ListView
                        val view = LayoutInflater.from(MainActivity.INSTANCE).inflate(R.layout.list_view_activity, null)
                        val slpBlockiesAddress = SLPWalletHelper.blockieAddressFromTokenId(walletHelper.tokenList[position]["tokenHash"]?: error(""))

                        val slpImage = view.findViewById<BlockiesIdenticon>(R.id.slpImage)
                        val slpIcon = view.findViewById<ImageView>(R.id.slpWithIcon)

                        object: Thread() {
                            override fun run()
                            {
                                try {
                                    val tokenHash = walletHelper.tokenList[position]["tokenHash"]
                                    val exists = MainActivity.INSTANCE.resources.getIdentifier("slp$tokenHash", "drawable", MainActivity.INSTANCE.packageName) != 0
                                    if(exists) {
                                        val drawable = MainActivity.INSTANCE.resources.getDrawable(MainActivity.INSTANCE.resources.getIdentifier("slp$tokenHash", "drawable", MainActivity.INSTANCE.packageName))
                                        MainActivity.INSTANCE.runOnUiThread {
                                            slpIcon.setImageDrawable(drawable)
                                            slpImage.visibility = View.GONE
                                            slpIcon.visibility = View.VISIBLE
                                        }
                                    } else {
                                        slpImage.setAddress(slpBlockiesAddress)
                                        slpImage.setCornerRadius(128f)
                                    }
                                } catch(e: Exception) {
                                    slpImage.setAddress(slpBlockiesAddress)
                                    slpImage.setCornerRadius(128f)
                                }
                            }
                        }.start()

                        // Initialize a TextView for ListView each Item
                        val text1 = view.findViewById<TextView>(R.id.text1)
                        val text2 = view.findViewById<TextView>(R.id.text2)
                        val text3 = view.findViewById<TextView>(R.id.text3)

                        // Set the text color of TextView (ListView Item)

                        text1.text = walletHelper.tokenList[position]["token"].toString()
                        text2.text = walletHelper.tokenList[position]["balance"].toString()
                        text3.text = walletHelper.tokenList[position]["tokenTicker"].toString()

                        if (nightModeEnabled) {
                            text1.setTextColor(Color.WHITE)
                            text2.setTextColor(Color.GRAY)
                            text3.setTextColor(Color.WHITE)
                        } else {
                            text1.setTextColor(Color.BLACK)
                            text3.setTextColor(Color.BLACK)
                        }

                        text2.ellipsize = TextUtils.TruncateAt.END
                        text2.maxLines = 1
                        text2.setSingleLine(true)
                        // Generate ListView Item using TextView
                        return view
                    }
                }
                MainActivity.INSTANCE.runOnUiThread {
                    slpList.adapter = itemsAdapter
                    slpList.refreshDrawableState()
                }
            } else {
                srlSLP.visibility = View.GONE
                no_tx_text_slp.visibility = View.VISIBLE
            }
        }
    }

    private fun setListViewShit(wallet: Wallet?) {
        if (wallet != null) {
            val txListFromWallet = wallet.getRecentTransactions(100, false)

            if (txListFromWallet != null && txListFromWallet.size != 0) {
                val txListFormatted = ArrayList<Map<String, String>>()
                MainActivity.INSTANCE.txList = ArrayList()

                val sizeToUse = if (txListFromWallet.size >= 100)
                    100
                else
                    txListFromWallet.size

                if(sizeToUse > 0) {
                    no_tx_text.visibility = View.GONE
                    srlHistory.visibility = View.VISIBLE

                    for (x in 0 until sizeToUse) {
                        val tx = txListFromWallet[x]
                        val confirmations = tx.confidence.depthInBlocks
                        val value = tx.getValue(wallet)
                        val datum = HashMap<String, String>()
                        var amountDbl = java.lang.Double.parseDouble(value.toPlainString().replace("-", ""))
                        val unit = MainActivity.INSTANCE.walletHelper.displayUnits
                        var amountStr = ""

                        if (value.isPositive) {
                            when (unit) {
                                MonetaryFormat.CODE_BTC -> {
                                    amountStr = formatBalance(amountDbl, "#,###.########")
                                }
                                MonetaryFormat.CODE_MBTC -> {
                                    amountDbl *= 1000
                                    amountStr = formatBalance(amountDbl, "#,###.#####")
                                }
                                MonetaryFormat.CODE_UBTC -> {
                                    amountDbl *= 1000000
                                    amountStr = formatBalance(amountDbl, "#,###.##")
                                }
                                "sats" -> {
                                    amountDbl *= 100000000
                                    amountStr = formatBalance(amountDbl, "#,###")
                                }
                            }

                            datum["action"] = "received"
                            val entry = String.format("%5s", amountStr)
                            datum["amount"] = entry
                            txListFormatted.add(datum)
                            MainActivity.INSTANCE.txList.add(tx)
                        }

                        if (value.isNegative) {
                            when (unit) {
                                MonetaryFormat.CODE_BTC -> {
                                    amountStr = formatBalance(amountDbl, "#,###.########")
                                }
                                MonetaryFormat.CODE_MBTC -> {
                                    amountDbl *= 1000
                                    amountStr = formatBalance(amountDbl, "#,###.#####")
                                }
                                MonetaryFormat.CODE_UBTC -> {
                                    amountDbl *= 1000000
                                    amountStr = formatBalance(amountDbl, "#,###.##")
                                }
                                "sats" -> {
                                    amountDbl *= 100000000
                                    amountStr = formatBalance(amountDbl, "#,###")
                                }
                            }

                            datum["action"] = "sent"
                            val entry = String.format("%5s", amountStr)
                            datum["amount"] = entry
                            txListFormatted.add(datum)
                            MainActivity.INSTANCE.txList.add(tx)
                        }

                        when {
                            confirmations == 0 -> datum["confirmations"] = "0/unconfirmed"
                            confirmations < 6 -> datum["confirmations"] = "$confirmations/6 confirmations"
                            else -> datum["confirmations"] = "6+ confirmations"
                        }
                    }

                    val itemsAdapter = object : SimpleAdapter(MainActivity.INSTANCE, txListFormatted, R.layout.transaction_list_cell, null, null) {
                        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                            // Get the Item from ListView
                            val view = LayoutInflater.from(MainActivity.INSTANCE).inflate(R.layout.transaction_list_cell, null)
                            val actionImg = view.findViewById<ImageView>(R.id.txAction)
                            val action = txListFormatted[position]["action"]

                            if(action == "sent")
                            {
                                actionImg.setBackgroundResource(R.drawable.send)
                            }
                            else if(action == "received")
                            {
                                actionImg.setBackgroundResource(R.drawable.receive)
                            }

                            // Initialize a TextView for ListView each Item
                            val text1 = view.findViewById(R.id.text1) as TextView
                            val text2 = view.findViewById(R.id.text2) as TextView

                            val amount = txListFormatted[position]["amount"]
                            val confirmations = txListFormatted[position]["confirmations"]
                            text1.text = amount.toString()
                            text2.text = confirmations.toString()
                            // Set the text color of TextView (ListView Item)

                            if (nightModeEnabled) {
                                text1.setTextColor(Color.WHITE)
                                text2.setTextColor(Color.GRAY)
                            } else {
                                text1.setTextColor(Color.BLACK)
                            }

                            text2.ellipsize = TextUtils.TruncateAt.END
                            text2.maxLines = 1
                            text2.setSingleLine(true)
                            // Generate ListView Item using TextView
                            return view
                        }
                    }
                    MainActivity.INSTANCE.runOnUiThread { txHistoryList.adapter = itemsAdapter }
                } else {
                    srlHistory.visibility = View.GONE
                    no_tx_text.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun formatBalance(amount: Double, pattern: String): String {
        val formatter = DecimalFormat(pattern, DecimalFormatSymbols(Locale.US))
        val formattedStr = formatter.format(amount)
        return "$formattedStr ${MainActivity.INSTANCE.walletHelper.displayUnits}"
    }

    private fun clickScanQR() {
        val qrHelper = QRHelper()
        qrHelper.startQRScan(MainActivity.INSTANCE)
    }

    fun displayRecipientAddress(recipientAddress: String?) {
        if (recipientAddress != null) {
            if (TextUtils.isEmpty(recipientAddress)) {
                tvRecipientAddress_AM.hint = MainActivity.INSTANCE.resources.getString(R.string.receiver)
            } else {
                tvRecipientAddress_AM.setText(recipientAddress)
            }
        } else {
            tvRecipientAddress_AM.text = null
            tvRecipientAddress_AM.hint = MainActivity.INSTANCE.resources.getString(R.string.receiver)
        }
    }

    fun showToastMessage(message: String) {
        Toast.makeText(MainActivity.INSTANCE, message, Toast.LENGTH_SHORT).show()
    }

    private fun showContactSelectionScreen()
    {
        val pickContact = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
        pickContact.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
        MainActivity.INSTANCE.startActivityForResult(pickContact, 14)
    }

    private fun setWalletInfo(recoverySeedStr: String) {
        this.xpub.text = MainActivity.INSTANCE.walletHelper.getXPUB()
        this.btnShowSeed.setOnClickListener { v -> showAlertDialog("Your recovery seed:", recoverySeedStr, "Hide") }
    }

    fun showAlertDialog(title: String, message: String, closePrompt: String)
    {
        val builder = androidx.appcompat.app.AlertDialog.Builder(MainActivity.INSTANCE, if (nightModeEnabled) R.style.AlertDialogDark else R.style.AlertDialogLight)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setCancelable(true)
        builder.setPositiveButton(closePrompt) { dialog, which -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.show()
        val msgTxt = alertDialog.findViewById<View>(android.R.id.message) as TextView
        msgTxt.movementMethod = LinkMovementMethod.getInstance()
    }

    @UiThread
    fun displayPercentage(percent: Int) {
        if (MainActivity.INSTANCE.walletHelper.downloading)
            syncPct.text = "Syncing... $percent%"
    }

    @UiThread
    fun displayMyBalance(myBalance: String) {
        MainActivity.INSTANCE.runOnUiThread {
            var balanceStr = myBalance
            balanceStr = balanceStr.replace(" BCH", "")
            var formatted = java.lang.Double.parseDouble(balanceStr)
            var balanceText = ""
            when (MainActivity.INSTANCE.walletHelper.displayUnits) {
                MonetaryFormat.CODE_BTC -> {
                    balanceText = formatBalance(formatted, "#,###.########")
                }
                MonetaryFormat.CODE_MBTC -> {
                    formatted *= 1000
                    balanceText = formatBalance(formatted, "#,###.#####")
                }
                MonetaryFormat.CODE_UBTC -> {
                    formatted *= 1000000
                    balanceText = formatBalance(formatted, "#,###.##")
                }
                "sats" -> {
                    formatted *= 100000000
                    balanceText = formatBalance(formatted, "#,###")
                }
            }

            balance.text = balanceText
            bchBalSend.text = balanceText
        }
    }

    private fun generateQR(textToConvert: String, viewID: Int, slp: Boolean) {
        try {
            val encoder = BarcodeEncoder()

            val qrCode = encoder.encodeBitmap(textToConvert, BarcodeFormat.QR_CODE, 1024, 1024)

            val coinLogo: Bitmap? = if (!slp)
                drawableToBitmap(MainActivity.INSTANCE.resources.getDrawable(R.drawable.logo_bch))
            else
                drawableToBitmap(MainActivity.INSTANCE.resources.getDrawable(R.drawable.logo_slp))

            val merge = overlayBitmapToCenter(qrCode, coinLogo!!)
            (MainActivity.INSTANCE.findViewById<View>(viewID) as ImageView).setImageBitmap(merge)
        } catch (e: WriterException) {
            e.printStackTrace()
        }
    }

    /*
    I'm absolutely terrible with Bitmap and image generation shit. Always have been.

    Shout-out to StackOverflow for some of this.
     */
    private fun overlayBitmapToCenter(bitmap1: Bitmap, bitmap2: Bitmap): Bitmap {
        val bitmap1Width = bitmap1.width
        val bitmap1Height = bitmap1.height
        val bitmap2Width = bitmap2.width
        val bitmap2Height = bitmap2.height

        val marginLeft = (bitmap1Width * 0.5 - bitmap2Width * 0.5).toFloat()
        val marginTop = (bitmap1Height * 0.5 - bitmap2Height * 0.5).toFloat()

        val overlayBitmap = Bitmap.createBitmap(bitmap1Width, bitmap1Height, bitmap1.config)
        val canvas = Canvas(overlayBitmap)
        canvas.drawBitmap(bitmap1, Matrix(), null)
        canvas.drawBitmap(bitmap2, marginLeft, marginTop, null)
        return overlayBitmap
    }

    private fun drawableToBitmap(drawable: Drawable): Bitmap? {
        val bitmap: Bitmap? = if (drawable.intrinsicWidth <= 0 || drawable.intrinsicHeight <= 0) {
            Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888) // Single color bitmap will be created of 1x1 pixel
        } else {
            Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        }

        if (drawable is BitmapDrawable) {
            if (drawable.bitmap != null) {
                return drawable.bitmap
            }
        }

        val canvas = Canvas(bitmap!!)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }

    private fun clearAmount() {
        amountText.text = null
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }

        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun refresh() {
        MainActivity.INSTANCE.walletHelper.serializedXpub = MainActivity.INSTANCE.walletHelper.wallet.watchingKey.serializePubB58(MainActivity.INSTANCE.walletHelper.parameters)

        if (showFiat) {
            object : Thread() {
                override fun run() {
                    val coinBal = java.lang.Double.parseDouble(MainActivity.INSTANCE.walletHelper.getBalance(MainActivity.INSTANCE.walletHelper.wallet).toPlainString())
                    val df = DecimalFormat("#,###.##", DecimalFormatSymbols(Locale.US))

                    val fiatBalances = when (fiat) {
                        "USD" -> {
                            val priceUsd = MainActivity.INSTANCE.netHelper.price
                            val balUsd = coinBal * priceUsd

                            "$" + df.format(balUsd)
                        }
                        "EUR" -> {
                            val priceEur = MainActivity.INSTANCE.netHelper.priceEur
                            val balEur = coinBal * priceEur

                            "€" + df.format(balEur)
                        }
                        "AUD" -> {
                            val priceAud = MainActivity.INSTANCE.netHelper.priceAud
                            val balAud = coinBal * priceAud

                            "AUD$" + df.format(balAud)
                        }
                        else -> ""
                    }

                    MainActivity.INSTANCE.runOnUiThread {
                        fiatBalTxt.text = fiatBalances
                        fiatBalTxtSend.text = fiatBalances
                    }
                }
            }.start()
        } else {
            fiatBalTxt.text = ""
            fiatBalTxtSend.text = ""
        }

        displayMyBalance(MainActivity.INSTANCE.walletHelper.getBalance(MainActivity.INSTANCE.walletHelper.wallet).toFriendlyString())

        val cashAccount = MainActivity.INSTANCE.prefs.getString("cashAccount", "")
        var cashEmoji = MainActivity.INSTANCE.prefs.getString("cashEmoji", "")

        if (cashAccount!!.contains("#???")) {
            cashEmoji = "?"
            val cashAcctPlain = cashAccount.replace("#???", "")
            MainActivity.INSTANCE.netHelper.initialAccountIdentityCheck(cashAcctPlain)
        }

        if(MainActivity.INSTANCE.walletHelper.slpWallet != null){
            this.displayCashAccount(MainActivity.INSTANCE.walletHelper.wallet, cashAccount)
            MainActivity.INSTANCE.walletHelper.getSLPWallet().refreshBalance()

            try
            {
                val bchBalance = MainActivity.INSTANCE.walletHelper.findBitcoinInSLPList()

                if (bchBalance != null) {
                    var formatted = java.lang.Double.parseDouble(bchBalance.amount.toPlainString())

                    when (MainActivity.INSTANCE.walletHelper.displayUnits) {
                        MonetaryFormat.CODE_BTC -> {
                            bchBalanceSLP.text = formatBalance(formatted, "#,###.########")
                        }
                        MonetaryFormat.CODE_MBTC -> {
                            formatted *= 1000
                            bchBalanceSLP.text = formatBalance(formatted, "#,###.#####")
                        }
                        MonetaryFormat.CODE_UBTC -> {
                            formatted *= 1000000
                            bchBalanceSLP.text = formatBalance(formatted, "#,###.##")
                        }
                        "sats" -> {
                            formatted *= 100000000
                            bchBalanceSLP.text = formatBalance(formatted, "#,###")
                        }
                    }
                }
            }
            catch(e: UninitializedPropertyAccessException)
            {
                e.printStackTrace()
            }
        }

        this.displayEmoji(cashEmoji!!)
        setArrayAdapter(MainActivity.INSTANCE.walletHelper.wallet)
    }

    companion object {
        var nightModeEnabled = false
        var showFiat = true
        var fiat = "USD"
    }
}
