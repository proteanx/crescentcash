package app.crescentcash.src

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import androidx.appcompat.app.AppCompatActivity
import android.view.View

import com.google.common.util.concurrent.ListenableFuture
import com.google.zxing.integration.android.IntentIntegrator

import org.bitcoinj.core.Transaction
import org.bitcoinj.protocols.payments.PaymentProtocolException
import org.bitcoinj.protocols.payments.PaymentSession
import org.bitcoinj.utils.MonetaryFormat

import java.text.DecimalFormat
import java.util.ArrayList
import java.util.concurrent.ExecutionException

import app.crescentcash.src.hash.HashHelper
import app.crescentcash.src.net.NetHelper
import app.crescentcash.src.ui.UIHelper
import app.crescentcash.src.utils.PermissionHelper
import app.crescentcash.src.wallet.SLPWalletHelper
import app.crescentcash.src.wallet.WalletHelper
import android.widget.Toast
import android.provider.ContactsContract
import android.view.View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
import android.view.View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
import org.bitcoinj.core.AddressFactory
import org.bitcoinj.core.Coin
import org.bitcoinj.core.ECKey
import org.bitcoinj.params.MainNetParams
import org.bitcoinj.wallet.SendRequest
import org.bitcoinj.wallet.Wallet


class MainActivity : AppCompatActivity() {
    lateinit var uiHelper: UIHelper
    lateinit var walletHelper: WalletHelper
    lateinit var netHelper: NetHelper

    lateinit var txList: ArrayList<Transaction>
    lateinit var prefs: SharedPreferences
    var started = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        started = false
        INSTANCE = this
        this.prefs = getSharedPreferences("app.crescentcash.src", Context.MODE_PRIVATE)
        UIHelper.nightModeEnabled = prefs.getBoolean("nightMode", false)
        UIHelper.showFiat = prefs.getBoolean("showFiat", true)
        UIHelper.fiat = prefs.getString("fiat", "USD") as String
        WalletHelper.addOpReturn = prefs.getBoolean("addOpReturn", true)
        WalletHelper.encrypted = prefs.getBoolean("useEncryption", false)
        WalletHelper.useTor = prefs.getBoolean("useTor", false)

        setTheme(if (UIHelper.nightModeEnabled) R.style.CrescentCashDark else R.style.CrescentCashLight)

        SLPWalletHelper.setAPIKey()

        this.setContentView(R.layout.activity_main)
        this.uiHelper = UIHelper()
        this.netHelper = NetHelper()
        this.walletHelper = WalletHelper()
        this.uiHelper.nightModeSwitch.isChecked = UIHelper.nightModeEnabled
        this.uiHelper.addOpReturnSwitch.isChecked = WalletHelper.addOpReturn
        this.uiHelper.showFiatSwitch.isChecked = UIHelper.showFiat
        this.uiHelper.encryptWalletSwitch.isChecked = WalletHelper.encrypted
        this.uiHelper.useTorSwitch.isChecked = WalletHelper.useTor

        isNewUser = this.prefs.getBoolean("isNewUser", true)

        if (isNewUser) {
            this.uiHelper.newuser.visibility = View.VISIBLE
        } else {
            val hasEmoji = this.prefs.getString("cashEmoji", "")

            if (hasEmoji != null) {
                if (hasEmoji == "?" || hasEmoji == "") {
                    object : Thread() {
                        override fun run() {
                            val cashAcctTx = MainActivity.INSTANCE.prefs.getString("cashAcctTx", "null") as String
                            walletHelper.registeredTxHash = cashAcctTx

                            if (cashAcctTx != "null") {
                                try {
                                    walletHelper.registeredBlockHash = netHelper.getTransactionData(walletHelper.registeredTxHash, "block_hash", "blockhash")

                                    if (walletHelper.registeredBlock != "???") {
                                        try {
                                            val emoji = HashHelper().getCashAccountEmoji(walletHelper.registeredBlockHash, walletHelper.registeredTxHash)
                                            prefs.edit().putString("cashEmoji", emoji).apply()
                                            uiHelper.myEmoji.text = emoji
                                        } catch (e: Exception) {
                                            uiHelper.myEmoji.text = "?"
                                        }

                                    } else {
                                        uiHelper.myEmoji.text = "?"
                                    }
                                } catch (e: NullPointerException) {
                                    e.printStackTrace()
                                    uiHelper.myEmoji.text = "?"
                                }
                            } else {
                                try {
                                    val cashAcctName = MainActivity.INSTANCE.prefs.getString("cashAccount", "") as String
                                    val cashAcctEmoji = netHelper.getCashAccountEmoji(cashAcctName)
                                    prefs.edit().putString("cashEmoji", cashAcctEmoji).apply()
                                    uiHelper.myEmoji.text = cashAcctEmoji
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                    runOnUiThread { uiHelper.showToastMessage("Error getting emoji") }
                                }

                            }
                        }
                    }.start()
                } else {
                    uiHelper.myEmoji.text = hasEmoji
                }
            }

            val hasSLPWallet = this.prefs.getBoolean("hasSLPWallet", false)

            if (hasSLPWallet)
                this.walletHelper.setupSLPWallet(null, true)

            this.walletHelper.setupWalletKit(null, "", false)
            this.uiHelper.displayDownloadContent(true)
            val cashAcct = MainActivity.INSTANCE.prefs.getString("cashAccount", "")!!

            if (cashAcct != "" && cashAcct.contains("#???")) {
                val plainName = cashAcct.replace("#???", "")
                this.walletHelper.registeredBlock = "???"
                this.netHelper.initialAccountIdentityCheck(plainName)

                this.walletHelper.timer = object : CountDownTimer(150000, 20) {

                    override fun onTick(millisUntilFinished: Long) {

                    }

                    override fun onFinish() {
                        try {
                            netHelper.checkForAccountIdentity(plainName)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }
                }.start()
            }

        }

        val permissionHelper = PermissionHelper()
        permissionHelper.askForPermissions(this, this)

        started = true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode != 14) {
            val scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
            if (scanResult != null) {
                if (this.uiHelper.sendSLPWindow.visibility != View.VISIBLE) {
                    val address = scanResult.contents
                    if (address != null) {
                        this.walletHelper.processScanOrPaste(address)
                    }
                } else {
                    val address = scanResult.contents
                    if (address != null) {
                        this.uiHelper.slpRecipientAddress.text = address
                    }
                }
            }
        }
        else
        {
            if(data != null) {
                if (data.data != null) {
                    val contactData = data.data
                    val c = contentResolver.query(contactData, null, null, null, null)
                    if (c!!.moveToFirst()) {
                        val phoneIndex = c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER)
                        val num = c.getString(phoneIndex)
                        uiHelper.displayRecipientAddress(num)
                        c.close()
                    }
                } else {
                    uiHelper.showToastMessage("No contact selected.")
                }
            } else {
                uiHelper.showToastMessage("No contact selected.")
            }
        }
    }

    override fun onBackPressed() {
        when {
            this.uiHelper.new_wallet.visibility == View.VISIBLE -> {
                this.uiHelper.new_wallet.visibility = View.GONE
                this.uiHelper.newuser.visibility = View.VISIBLE
            }
            this.uiHelper.restore_wallet.visibility == View.VISIBLE -> {
                this.uiHelper.restore_wallet.visibility = View.GONE
                this.uiHelper.newuser.visibility = View.VISIBLE
            }
            this.uiHelper.sendWindow.visibility == View.VISIBLE -> {
                this.uiHelper.sendWindow.visibility = View.GONE
                this.uiHelper.clearSend()
            }
            this.uiHelper.sendSLPWindow.visibility == View.VISIBLE -> {
                this.uiHelper.sendSLPWindow.visibility = View.GONE
                this.uiHelper.clearSend()
            }
            this.uiHelper.receiveWindow.visibility == View.VISIBLE -> this.uiHelper.receiveWindow.visibility = View.GONE
            this.uiHelper.receiveWindowSLP.visibility == View.VISIBLE -> this.uiHelper.receiveWindowSLP.visibility = View.GONE
            this.uiHelper.txInfo.visibility == View.VISIBLE -> this.uiHelper.txInfo.visibility = View.GONE
            this.uiHelper.encryptScreen.visibility == View.VISIBLE -> {
                WalletHelper.encrypted = false
                this.uiHelper.encryptWalletSwitch.isChecked = WalletHelper.encrypted
                MainActivity.INSTANCE.prefs.edit().putBoolean("useEncryption", WalletHelper.encrypted).apply()
                this.uiHelper.encryptScreen.visibility = View.GONE
            }
            this.uiHelper.walletSettings.visibility == View.VISIBLE -> {
                this.walletHelper.maximumAutomaticSend = this.uiHelper.autoSendAmount.text.toString().toFloat()
                this.prefs.edit().putFloat("maximumAutomaticSend", this.walletHelper.maximumAutomaticSend).apply()
                this.uiHelper.walletSettings.visibility = View.GONE
            }
            this.uiHelper.slpBalancesWindow.visibility == View.VISIBLE -> this.uiHelper.slpBalancesWindow.visibility = View.GONE
            this.uiHelper.bitcoinWindow.visibility == View.VISIBLE -> this.uiHelper.bitcoinWindow.visibility = View.GONE
            else -> {
                val intent = Intent(Intent.ACTION_MAIN)
                intent.addCategory(Intent.CATEGORY_HOME)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }
        }
    }

    companion object {
        lateinit var INSTANCE: MainActivity
        var isNewUser = true
    }
}
